from prettytable import PrettyTable
from prerrequisitos import diccionarioPrerrequisitos
from getpass import getpass

diccionarioCursos = {
	"aprobados": {},
	"desaprobados": {}
}

demandaCursos = {}

def procesarArchivo():
	f = open("BD.csv","r")
	for linea in f.readlines():
		[codAlumno, ciclo, codCurso, aprobado, codAprobado, semestre, terminal, futuro] = linea.split(',')
		if codAprobado == '0' or codAprobado == '2':
			try:
				diccionarioCursos["desaprobados"][codCurso] += 1
			except:
				diccionarioCursos["desaprobados"][codCurso] = 0
		else :
			try:
				diccionarioCursos["aprobados"][codCurso] += 1
			except:
				diccionarioCursos["aprobados"][codCurso] = 0
	f.close()

def procesarDemanda():
	for codCurso, preqs in diccionarioPrerrequisitos.items():
		try:
			demandaCursos[codCurso] = diccionarioCursos["desaprobados"][codCurso] + minimo(preqs['pre'])
		except:
			try:
				demandaCursos[codCurso] = 0 + minimo(preqs['pre'])
			except:
				try:
					demandaCursos[codCurso] = diccionarioCursos["desaprobados"][codCurso] + 0
				except:
					demandaCursos[codCurso] = 0

def minimo(array):
	minNumber = 10000
	for i in array:
		if diccionarioCursos['aprobados'][i] < minNumber:
			minNumber = diccionarioCursos['aprobados'][i]
	return i

def imprimirDemandaCursos():
	table = PrettyTable()
	table.field_names = ['Ciclo', 'Curso', 'Demanda']
	cicloActual = ''
	for key, value in demandaCursos.items(): 
		if cicloActual != str(diccionarioPrerrequisitos[key]['ciclo']):
			cicloActual = str(diccionarioPrerrequisitos[key]['ciclo'])
			table.add_row(['-------', '-------', '-------'])
			table.add_row([cicloActual, key, value])
		else:
			table.add_row(['', key, value])
	print(table)

def mostrarLogin():
	print('----------------------------')
	print('---* INGRESO AL SISTEMA *---')
	print('Usuario', end=': ')
	usuario = input()
	print('Contraseña', end=': ')
	password = getpass()
	iniciarSesion(usuario, password)

def iniciarSesion(usuario, password):
	if usuario == 'docente' and password == 'unac123':
		mostrarMenu()
	else:
		print('-- INGRESO NO AUTORIZADO --')

def mostrarMenu():
	print('--------------------------')
	print('---* MENÚ DE OPCIONES *---')
	print('\t\t 1. Consulta por alumno')
	print('\t\t 2. Nuevo Registro')
	print('\t\t 3. Modificación de registro')
	print('\t\t 4. Eliminación de registro')
	print('\t\t 5. Reporte por demanda')
	print('\t\t 6. Salir')
	leerOpcion()

def leerOpcion():
	salir = False
	print('Ingrese opcion', end=': ')
	opcion = input()
	if opcion == '1':
		mostrarConsulta()
	elif opcion == '2':
		mostrarRegistro()
	elif opcion == '3':
		mostrarModificacion()
	elif opcion == '4':
		mostrarEliminacion()
	elif opcion == '5':
		mostrarReporte()
	elif opcion == '6':
		salir = True
	else:
		if not salir:
			print('Opción no encontrada. Ingrese nuevamente')
			leerOpcion()

def mostrarConsulta():
	table = PrettyTable()
	table.field_names = ['CODIGO', 'CICLO' , 'COD. CURSO', 'RESULTADO', 'SEMESTRE', 'FUTURO']
	codigoEstudiante = input('Ingrese código de estudiante a buscar: ')
	f = open("BD.csv","r")
	for linea in f.readlines():
		columnas = linea.split(';')
		if codigoEstudiante == columnas[0]:
			table.add_row([columnas[0], columnas[1], columnas[2], columnas[7], columnas[9], columnas[11]])
	print(table)
	f.close()
	mostrarMenu()

def mostrarRegistro():
	print('Registro Nuevo')
	alumnos = []
	f = open("BD.csv","a")
	n = int(input("Ingrese la cantidad de registros a guardar:"))
	for i in range(n):
		alumnos.append({
			"codigoAlumno": input('Ingrese código de alumno:'),
			"ciclo": input('Ingrese ciclo:'),
			"codigoCurso": input('Ingrese código del curso:'),
			"resultado": input('Ingrese resultado:'),
			"semestre": input('Ingrese semestre:'),
			"futuro": input('Ingrese futuro')
		})
	for alumno in alumnos:
		f.write(f"{alumno['codigoAlumno']};${alumno['ciclo']};${alumno['codigoCurso']};${alumno['resultado']};${alumno['semestre']};${alumno['resultado']};${alumno['futuro']}\n")
	f.close()
	mostrarMenu()

def mostrarModificacion():
	codigoAlumno = input('Ingrese código de alumno a editar: ')
	ciclo = input('Ingrese ciclo: ')
	codigoCurso = input('Ingrese código del curso: ')
	resultado = input('Ingrese resultado: ')
	semestre = input('Ingrese semestre: ')
	futuro = input('Ingrese futuro: ')
	f = open("BD.csv","r")
	lineas = f.readlines()
	f.close()
	f = open('BD.csv', 'w')
	encontrado = False
	cont = 0
	for linea in lineas:
		columnas = linea.split(';')
		print(columnas[0], columnas[1], columnas[2])
		print(codigoAlumno, ciclo, codigoCurso)
		if codigoAlumno == columnas[0] and ciclo == columnas[1] and codigoCurso == columnas[2]:
			print('Registro encontrado:\n', columnas.join(' - '))
			print('Se modificará a:', codigoAlumno, ' - ', ciclo, ' - ', codigoCurso, ' - ', resultado, ' - ', semestre, ' - ', futuro)
			encontrado = True
			lineas.remove(linea)
			lineas.insert(cont, f'${codigoAlumno};${ciclo};${codigoCurso};${resultado};${semestre};${futuro}\n')
		cont += 1
	if not encontrado:
		print('Registro no encontrado')
	f.writelines(lineas)
	f.close()
	mostrarMenu()

def mostrarReporte():
	imprimirDemandaCursos()