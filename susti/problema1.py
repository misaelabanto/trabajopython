personas = []

def getPersona():
    nombre = input('Ingrese nombre: ')
    telefono = input('Ingrese teléfono: ')
    return (nombre, telefono)

def main():
    n = int(input('Ingrese número de personas: '))
    for i in range(n):
        nombre, telefono = getPersona()
        personas.append({
            'nombre': nombre,
            'telefono': telefono
        })
    buscarPorDosUltimosDigitos()

def buscarPorDosUltimosDigitos():
    resultados = []
    dosUltimosDigitos = input('Ingrese últimos dos dígitos: ')
    print('Digitos buscados: ' + dosUltimosDigitos)
    for persona in personas:
        if persona['telefono'][-2:] == dosUltimosDigitos:
            print('Persona encontrada: ' + persona['nombre'])
            resultados.append(persona)
    
    lon = len(resultados)
    for i in range(lon - 1):
        for j in range(i+1, lon):
            if resultados[i]['nombre'] > resultados[j]['nombre']:
                aux = resultados[i]
                resultados[i] = resultados[j]
                resultados[j] = aux

    imprimirResultados(resultados)
    
def imprimirResultados(resultados):
    print('Nombre completo', 'Telefono')
    for resultado in resultados:
        print(resultado['nombre'] + '\t' +  resultado['telefono'])

main()
