def filtrarRegistros(letras, tallaMinima, pesoMinimo):
    archivoCopia = open('copia.dat', 'w+')
    archivoPersonas = open('PERSONA.DAT')
    for persona in archivoPersonas.readlines():
        [codigo, nombre, talla, peso] = persona.split(',')
        if nombre[:1] in letras and float(talla) > tallaMinima and float(peso) > pesoMinimo:
            archivoCopia.write(persona + '\n')
    archivoCopia.close()
    archivoPersonas.close()

def main():
    letras, tallaMinima, pesoMinimo = leerOpcion()
    filtrarRegistros(letras, tallaMinima, pesoMinimo)
    print('Registros filtrados con éxito y guardados en copia.dat')

def leerOpcion():
    opcion = input('¿Desea cambiar los valores por defecto? (S/N)')
    if opcion == 'S' or opcion == 's':
        letras = input('Ingrese las letras iniciales que desea (separadas por coma): ')
        tallaMinima = input('Ingrese talla mínima: ')
        pesoMinimo = input('Ingrese peso mínimo: ')
        return(letras.split(','), float(tallaMinima), float(pesoMinimo))
    elif opcion == 'N' or opcion == 'n':
        return(['A', 'C', 'G'], 160, 50)
    else:
        print('Opción no reconocida, vuelva a intentarlo')
        leerOpcion()

main()
