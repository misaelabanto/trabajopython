pesos = []
pesosInvertidos = []

def imprimirMenu():
    print('Seleccione Opción: ')
    print('1. Calcular pesos invertidos')
    print('2. Agregar nuevo empleado')
    leerOpcion()

def leerOpcion():
    opcion = input('Ingrese opcion: ')
    if opcion == '1':
        iniciarIntercambioPesos()
    elif opcion == '2':
        registrarNuevoEmpleado()
    else: 
        print('No se reconoce la opción. Intente nuevamente')
        leerOpcion()

def leerCampo(nombreCampo, longitud, ob = '='):
    campo = input('Ingrese ' + nombreCampo +': ')
    if len(campo) == longitud and ob == '=':
        return campo
    elif len(campo) < longitud and ob == '<':
        return campo
    else:
        print('¡' + nombreCampo + ' debe ser ' + ob + ' ' + str(longitud) + '!')
        return leerCampo(nombreCampo, longitud)


def leerReal(nombreReal):
    realStr = input('Ingrese ' + nombreReal + ': ')
    try:
        real = float(realStr)
        if real < 0:
            print('El ' + nombreReal + ' debe ser mayor que cero')
            return leerReal(nombreReal)
        else:
            return realStr
    except:
        print('¡El código no es un real')
        return leerReal(nombreReal)

def registrarNuevoEmpleado():
    print('Registrar nuevo empleado')
    codigo = leerCampo('Código', 4, '=')
    nombre = leerCampo('Nombre', 20, '<')
    talla = leerReal('Talla')
    peso = leerReal('Peso')

    archivoPersonas = open('PERSONA.DAT', 'a+')
    archivoPersonas.write('%s,%s,%s,%s\n'%(codigo, nombre, talla, peso))
    print('Archivo guardado con éxito')
    archivoPersonas.close()
    imprimirMenu()

def iniciarIntercambioPesos():
    global pesos
    global pesosInvertidos
    pesos = []
    pesosInvertidos = []
    pesos = leerArchivo()
    print('Pesos leídos: ', end='')
    print(pesos)
    intercambiaPesos(pesos)
    print('Pesos invertidos: ', end='')
    print(pesosInvertidos)
    imprimirMenu()
def leerArchivo():
    archivoPersonas = open('PERSONA.DAT')
    for persona in archivoPersonas.readlines():
        print(persona)
        [codigo, nombre, talla, peso] = persona.split(',')
        pesos.append(float(peso))
    archivoPersonas.close()
    return pesos

def intercambiaPesos(pesos):
    global pesosInvertidos
    if len(pesos) == 0:
        return
    else: 
        pesosInvertidos.append(pesos[len(pesos)-1:][0])
        intercambiaPesos(pesos[:len(pesos)-1])

def main():
    imprimirMenu()

main()
